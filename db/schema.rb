# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_13_164243) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "equipamentos", force: :cascade do |t|
    t.string "nome"
    t.string "descricao"
    t.string "status"
    t.bigint "tipo_equipamento_id"
    t.string "classificacao"
    t.string "flag_classificacao"
    t.string "identificacao"
    t.string "ip"
    t.string "mac"
    t.string "geolocalizacao"
    t.datetime "data_inicio"
    t.datetime "data_fim"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tipo_equipamento_id"], name: "index_equipamentos_on_tipo_equipamento_id"
  end

  create_table "registroprazos", force: :cascade do |t|
    t.string "nome"
    t.string "descricao"
    t.string "contador"
    t.string "namespace"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_documentos", force: :cascade do |t|
    t.string "nome"
    t.boolean "ativo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_equipamentos", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "equipamentos", "tipo_equipamentos"
end
