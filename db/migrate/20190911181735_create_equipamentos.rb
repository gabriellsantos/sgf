class CreateEquipamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :equipamentos do |t|
      t.string :nome
      t.string :descricao
      t.string :status
      t.references :tipo_equipamento, foreign_key: true
      t.string :classificacao
      t.string :flag_classificacao
      t.string :identificacao
      t.string :ip
      t.string :mac
      t.string :geolocalizacao
      t.datetime :data_inicio
      t.datetime :data_fim

      t.timestamps
    end
  end
end
