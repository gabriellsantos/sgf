class CreateTipoDocumentos < ActiveRecord::Migration[5.2]
  def change
    create_table :tipo_documentos do |t|
      t.string :nome
      t.boolean :ativo

      t.timestamps
    end
  end
end
