class CreateTipoEquipamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :tipo_equipamentos do |t|
      t.string :nome

      t.timestamps
    end
  end
end
