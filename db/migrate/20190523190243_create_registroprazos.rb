class CreateRegistroprazos < ActiveRecord::Migration[5.2]
  def change
    create_table :registroprazos do |t|
      t.string :nome
      t.string :descricao
      t.string :contador
      t.string :namespace

      t.timestamps
    end
  end
end
