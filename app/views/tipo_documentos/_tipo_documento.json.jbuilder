json.extract! tipo_documento, :id, :nome, :ativo, :created_at, :updated_at
json.url tipo_documento_url(tipo_documento, format: :json)
