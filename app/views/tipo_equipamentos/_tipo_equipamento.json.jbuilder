json.extract! tipo_equipamento, :id, :nome, :created_at, :updated_at
json.url tipo_equipamento_url(tipo_equipamento, format: :json)
