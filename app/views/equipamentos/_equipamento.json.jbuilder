json.extract! equipamento, :id, :nome, :descricao, :status, :tipo_equipamento_id, :classificacao, :flag_classificacao, :identificacao, :ip, :mac, :geolocalizacao, :data_inicio, :data_fim, :created_at, :updated_at
json.url equipamento_url(equipamento, format: :json)
