json.extract! registroprazo, :id, :nome, :descricao, :contador, :namespace, :created_at, :updated_at
json.url registroprazo_url(registroprazo, format: :json)
