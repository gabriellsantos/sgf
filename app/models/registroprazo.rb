# == Schema Information
#
# Table name: registroprazos
#
#  id         :bigint           not null, primary key
#  contador   :string
#  descricao  :string
#  namespace  :string
#  nome       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Registroprazo < ApplicationRecord
end
