# == Schema Information
#
# Table name: tipo_documentos
#
#  id         :bigint           not null, primary key
#  nome       :string
#  ativo      :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TipoDocumento < ApplicationRecord

    validates :nome, presence: true, length: { maximum: 255}, uniqueness: true


    #Método que retorna o nome do objeto instanciado
    def to_s
        self.nome
    end

end
