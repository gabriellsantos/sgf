# == Schema Information
#
# Table name: equipamentos
#
#  id                  :bigint           not null, primary key
#  classificacao       :string
#  data_fim            :datetime
#  data_inicio         :datetime
#  descricao           :string
#  flag_classificacao  :string
#  geolocalizacao      :string
#  identificacao       :string
#  ip                  :string
#  mac                 :string
#  nome                :string
#  status              :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo_equipamento_id :bigint
#
# Indexes
#
#  index_equipamentos_on_tipo_equipamento_id  (tipo_equipamento_id)
#
# Foreign Keys
#
#  fk_rails_...  (tipo_equipamento_id => tipo_equipamentos.id)
#

class Equipamento < ApplicationRecord
  belongs_to :tipo_equipamento, optional: true

  extend Enumerize
  extend ActiveModel::Naming

  enumerize :status, in: %w(autorizado nao_autorizado bloqueado movimentado recebido), i18n_scope: "equipamento.status", default: :nao_autorizado, predicates: true
  enumerize :classificacao, in: %w(movel fixo), i18n_scope: "equipamento.classificacao", predicates: true
  enumerize :flag_classificacao, in: %w(proprio institucional), i18n_scope: "equipamento.flag_classificacao", predicates: true

  has_paper_trail
end
