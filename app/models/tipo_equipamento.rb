# == Schema Information
#
# Table name: tipo_equipamentos
#
#  id         :bigint           not null, primary key
#  nome       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TipoEquipamento < ApplicationRecord
  validates :nome, presence: true, uniqueness: true, length: { maximum: 255}
  has_many :equipamentos
end
