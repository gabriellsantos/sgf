# == Schema Information
#
# Table name: equipamentos
#
#  id                  :bigint           not null, primary key
#  classificacao       :string
#  data_fim            :datetime
#  data_inicio         :datetime
#  descricao           :string
#  flag_classificacao  :string
#  geolocalizacao      :string
#  identificacao       :string
#  ip                  :string
#  mac                 :string
#  nome                :string
#  status              :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo_equipamento_id :bigint
#
# Indexes
#
#  index_equipamentos_on_tipo_equipamento_id  (tipo_equipamento_id)
#
# Foreign Keys
#
#  fk_rails_...  (tipo_equipamento_id => tipo_equipamentos.id)
#

class EquipamentosController < ApplicationController
  before_action :set_paper_trail_whodunnit

  before_action :set_equipamento, only: [:edit, :update, :destroy, :autorizar, :show]
  before_action :set_tipos, only: [:show, :new, :edit, :create, :update]
  before_action :set_classificacoes, only: [:new, :edit, :create, :update]
  before_action :set_flags_classificacao, only: [:new, :edit, :create, :update]

  # GET /equipamentos
  # GET /equipamentos.json
  def index
    @equipamentos = Equipamento.where(status: [:autorizado, :nao_autorizado, :bloqueado]).order(updated_at: :desc)
  end

  # GET /equipamentos/1
  # GET /equipamentos/1.json
  def show
  end

  # GET /equipamentos/new
  def new
    @equipamento = Equipamento.new
  end

  # GET /equipamentos/1/edit
  def edit
  end

  # POST /equipamentos
  # POST /equipamentos.json
  def create
    @equipamento = Equipamento.new(equipamento_params)

    if @equipamento.save
      if autorizar?
        redirect_to equipamento_autorizar_path(@equipamento)
      else
        respond_to do |format|
          format.html {redirect_to equipamentos_path, notice: 'Equipamento was successfully created.'}
          format.json {render :show, status: :created, location: @equipamento}
        end
      end
    else
      respond_to do |format|
        format.html {render :new}
        format.json {render json: @equipamento.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /equipamentos/1
  # PATCH/PUT /equipamentos/1.json
  def update
    if autorizar? and data_inicio? and data_fim?
      @equipamento.status = :autorizado
    end

    respond_to do |format|
      if @equipamento.update(equipamento_params)
        if @equipamento.status.autorizado?
          format.html {redirect_to equipamentos_path, notice: 'Equipamento autorizado com sucesso!'}
          format.json {render :show, status: :ok, location: @equipamento}
        else
          format.html {redirect_to equipamento_path, notice: 'Equipamento salvo com sucesso!'}
          format.json {render :show, status: :ok, location: @equipamento}
        end
      else
        format.html {render :edit}
        format.json {render json: @equipamento.errors, status: :unprocessable_entity}
      end
    end

    # respond_to do |format|
    #   if @equipamento.update(equipamento_params)
    #     format.html {redirect_to @equipamento, notice: 'Equipamento atualizado com sucesso!'}
    #     format.json {render :show, status: :ok, location: @equipamento}
    #   else
    #     format.html {render :edit}
    #     format.json {render json: @equipamento.errors, status: :unprocessable_entity}
    #   end
    # end
  end

  # DELETE /equipamentos/1
  # DELETE /equipamentos/1.json
  def destroy
    @equipamento.destroy
    respond_to do |format|
      format.html {redirect_to equipamentos_url, notice: 'Equipamento was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  def autorizar
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_equipamento
    @equipamento = Equipamento.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def equipamento_params
    params.require(:equipamento).permit(:nome,
                                        :descricao,
                                        :status,
                                        :tipo_equipamento_id,
                                        :classificacao,
                                        :flag_classificacao,
                                        :identificacao,
                                        :ip,
                                        :mac,
                                        :geolocalizacao,
                                        :data_inicio,
                                        :data_fim)
  end

  def set_tipos
    @tipos = TipoEquipamento.order(:nome)
  end

  def set_classificacoes
    @classificacoes = Equipamento.classificacao.options
  end

  def set_flags_classificacao
    @flags_classificacao = Equipamento.flag_classificacao.options
  end

  def autorizar?
    if params[:autorizar] == 'true'
      return true
    end
    false
  end

  def data_inicio?
    params[:equipamento]['data_inicio(3i)'].present? and
        params[:equipamento]['data_inicio(2i)'].present? and
        params[:equipamento]['data_inicio(1i)'].present? and
        params[:equipamento]['data_inicio(4i)'].present? and
        params[:equipamento]['data_inicio(5i)'].present?
  end

  def data_fim?
    params[:equipamento]['data_fim(3i)'].present? and
        params[:equipamento]['data_fim(2i)'].present? and
        params[:equipamento]['data_fim(1i)'].present? and
        params[:equipamento]['data_fim(4i)'].present? and
        params[:equipamento]['data_fim(5i)'].present?
  end

end
