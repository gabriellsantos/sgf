# == Schema Information
#
# Table name: registroprazos
#
#  id         :bigint           not null, primary key
#  contador   :string
#  descricao  :string
#  namespace  :string
#  nome       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RegistroprazosController < ApplicationController
  before_action :set_registroprazo, only: [:show, :edit, :update, :destroy]

  # GET /registroprazos
  # GET /registroprazos.json
  def index
    @registroprazos = Registroprazo.all
  end

  # GET /registroprazos/1
  # GET /registroprazos/1.json
  def show
  end

  # GET /registroprazos/new
  def new
    @registroprazo = Registroprazo.new
  end

  # GET /registroprazos/1/edit
  def edit
  end

  # POST /registroprazos
  # POST /registroprazos.json
  def create
    @registroprazo = Registroprazo.new(registroprazo_params)

    respond_to do |format|
      if @registroprazo.save
        format.html { redirect_to @registroprazo, notice: 'Registroprazo was successfully created.' }
        format.json { render :show, status: :created, location: @registroprazo }
      else
        format.html { render :new }
        format.json { render json: @registroprazo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /registroprazos/1
  # PATCH/PUT /registroprazos/1.json
  def update
    respond_to do |format|
      if @registroprazo.update(registroprazo_params)
        format.html { redirect_to @registroprazo, notice: 'Registroprazo was successfully updated.' }
        format.json { render :show, status: :ok, location: @registroprazo }
      else
        format.html { render :edit }
        format.json { render json: @registroprazo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registroprazos/1
  # DELETE /registroprazos/1.json
  def destroy
    @registroprazo.destroy
    respond_to do |format|
      format.html { redirect_to registroprazos_url, notice: 'Registroprazo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_registroprazo
      @registroprazo = Registroprazo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registroprazo_params
      params.require(:registroprazo).permit(:nome, :descricao, :contador, :namespace)
    end
end
