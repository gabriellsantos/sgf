module ApplicationHelper

	def icon(icon_name)
		raw '<i class="material-icons">'+icon_name+'</i>'
	end
end
