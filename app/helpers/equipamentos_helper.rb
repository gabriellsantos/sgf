# == Schema Information
#
# Table name: equipamentos
#
#  id                  :bigint           not null, primary key
#  classificacao       :string
#  data_fim            :datetime
#  data_inicio         :datetime
#  descricao           :string
#  flag_classificacao  :string
#  geolocalizacao      :string
#  identificacao       :string
#  ip                  :string
#  mac                 :string
#  nome                :string
#  status              :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  tipo_equipamento_id :bigint
#
# Indexes
#
#  index_equipamentos_on_tipo_equipamento_id  (tipo_equipamento_id)
#
# Foreign Keys
#
#  fk_rails_...  (tipo_equipamento_id => tipo_equipamentos.id)
#

module EquipamentosHelper
end
