require "application_system_test_case"

class EquipamentosTest < ApplicationSystemTestCase
  setup do
    @equipamento = equipamentos(:one)
  end

  test "visiting the index" do
    visit equipamentos_url
    assert_selector "h1", text: "Equipamentos"
  end

  test "creating a Equipamento" do
    visit equipamentos_url
    click_on "New Equipamento"

    fill_in "Classificacao", with: @equipamento.classificacao
    fill_in "Data fim", with: @equipamento.data_fim
    fill_in "Data inicio", with: @equipamento.data_inicio
    fill_in "Descricao", with: @equipamento.descricao
    fill_in "Flag classificacao", with: @equipamento.flag_classificacao
    fill_in "Geolocalizacao", with: @equipamento.geolocalizacao
    fill_in "Identificacao", with: @equipamento.identificacao
    fill_in "Ip", with: @equipamento.ip
    fill_in "Mac", with: @equipamento.mac
    fill_in "Nome", with: @equipamento.nome
    fill_in "Status", with: @equipamento.status
    fill_in "Tipo equipamento", with: @equipamento.tipo_equipamento_id
    click_on "Create Equipamento"

    assert_text "Equipamento was successfully created"
    click_on "Back"
  end

  test "updating a Equipamento" do
    visit equipamentos_url
    click_on "Edit", match: :first

    fill_in "Classificacao", with: @equipamento.classificacao
    fill_in "Data fim", with: @equipamento.data_fim
    fill_in "Data inicio", with: @equipamento.data_inicio
    fill_in "Descricao", with: @equipamento.descricao
    fill_in "Flag classificacao", with: @equipamento.flag_classificacao
    fill_in "Geolocalizacao", with: @equipamento.geolocalizacao
    fill_in "Identificacao", with: @equipamento.identificacao
    fill_in "Ip", with: @equipamento.ip
    fill_in "Mac", with: @equipamento.mac
    fill_in "Nome", with: @equipamento.nome
    fill_in "Status", with: @equipamento.status
    fill_in "Tipo equipamento", with: @equipamento.tipo_equipamento_id
    click_on "Update Equipamento"

    assert_text "Equipamento was successfully updated"
    click_on "Back"
  end

  test "destroying a Equipamento" do
    visit equipamentos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Equipamento was successfully destroyed"
  end
end
