require "application_system_test_case"

class RegistroprazosTest < ApplicationSystemTestCase
  setup do
    @registroprazo = registroprazos(:one)
  end

  test "visiting the index" do
    visit registroprazos_url
    assert_selector "h1", text: "Registroprazos"
  end

  test "creating a Registroprazo" do
    visit registroprazos_url
    click_on "New Registroprazo"

    fill_in "Contador", with: @registroprazo.contador
    fill_in "Descricao", with: @registroprazo.descricao
    fill_in "Namespace", with: @registroprazo.namespace
    fill_in "Nome", with: @registroprazo.nome
    click_on "Create Registroprazo"

    assert_text "Registroprazo was successfully created"
    click_on "Back"
  end

  test "updating a Registroprazo" do
    visit registroprazos_url
    click_on "Edit", match: :first

    fill_in "Contador", with: @registroprazo.contador
    fill_in "Descricao", with: @registroprazo.descricao
    fill_in "Namespace", with: @registroprazo.namespace
    fill_in "Nome", with: @registroprazo.nome
    click_on "Update Registroprazo"

    assert_text "Registroprazo was successfully updated"
    click_on "Back"
  end

  test "destroying a Registroprazo" do
    visit registroprazos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Registroprazo was successfully destroyed"
  end
end
