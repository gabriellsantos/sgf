require "application_system_test_case"

class TipoEquipamentosTest < ApplicationSystemTestCase
  setup do
    @tipo_equipamento = tipo_equipamentos(:one)
  end

  test "visiting the index" do
    visit tipo_equipamentos_url
    assert_selector "h1", text: "Tipo Equipamentos"
  end

  test "creating a Tipo equipamento" do
    visit tipo_equipamentos_url
    click_on "New Tipo Equipamento"

    fill_in "Nome", with: @tipo_equipamento.nome
    click_on "Create Tipo equipamento"

    assert_text "Tipo equipamento was successfully created"
    click_on "Back"
  end

  test "updating a Tipo equipamento" do
    visit tipo_equipamentos_url
    click_on "Edit", match: :first

    fill_in "Nome", with: @tipo_equipamento.nome
    click_on "Update Tipo equipamento"

    assert_text "Tipo equipamento was successfully updated"
    click_on "Back"
  end

  test "destroying a Tipo equipamento" do
    visit tipo_equipamentos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tipo equipamento was successfully destroyed"
  end
end
