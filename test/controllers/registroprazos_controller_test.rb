require 'test_helper'

class RegistroprazosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @registroprazo = registroprazos(:one)
  end

  test "should get index" do
    get registroprazos_url
    assert_response :success
  end

  test "should get new" do
    get new_registroprazo_url
    assert_response :success
  end

  test "should create registroprazo" do
    assert_difference('Registroprazo.count') do
      post registroprazos_url, params: { registroprazo: { contador: @registroprazo.contador, descricao: @registroprazo.descricao, namespace: @registroprazo.namespace, nome: @registroprazo.nome } }
    end

    assert_redirected_to registroprazo_url(Registroprazo.last)
  end

  test "should show registroprazo" do
    get registroprazo_url(@registroprazo)
    assert_response :success
  end

  test "should get edit" do
    get edit_registroprazo_url(@registroprazo)
    assert_response :success
  end

  test "should update registroprazo" do
    patch registroprazo_url(@registroprazo), params: { registroprazo: { contador: @registroprazo.contador, descricao: @registroprazo.descricao, namespace: @registroprazo.namespace, nome: @registroprazo.nome } }
    assert_redirected_to registroprazo_url(@registroprazo)
  end

  test "should destroy registroprazo" do
    assert_difference('Registroprazo.count', -1) do
      delete registroprazo_url(@registroprazo)
    end

    assert_redirected_to registroprazos_url
  end
end
