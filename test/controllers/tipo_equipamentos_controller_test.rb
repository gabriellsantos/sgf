require 'test_helper'

class TipoEquipamentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_equipamento = tipo_equipamentos(:one)
  end

  test "should get index" do
    get tipo_equipamentos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_equipamento_url
    assert_response :success
  end

  test "should create tipo_equipamento" do
    assert_difference('TipoEquipamento.count') do
      post tipo_equipamentos_url, params: { tipo_equipamento: { nome: @tipo_equipamento.nome } }
    end

    assert_redirected_to tipo_equipamento_url(TipoEquipamento.last)
  end

  test "should show tipo_equipamento" do
    get tipo_equipamento_url(@tipo_equipamento)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_equipamento_url(@tipo_equipamento)
    assert_response :success
  end

  test "should update tipo_equipamento" do
    patch tipo_equipamento_url(@tipo_equipamento), params: { tipo_equipamento: { nome: @tipo_equipamento.nome } }
    assert_redirected_to tipo_equipamento_url(@tipo_equipamento)
  end

  test "should destroy tipo_equipamento" do
    assert_difference('TipoEquipamento.count', -1) do
      delete tipo_equipamento_url(@tipo_equipamento)
    end

    assert_redirected_to tipo_equipamentos_url
  end
end
