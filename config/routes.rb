Rails.application.routes.draw do
  resources :equipamentos

  get 'equipamentos/autorizar/:id' => 'equipamentos#autorizar', as: 'equipamento_autorizar'
  # namespace :equipamentos do
  #   get 'autorizar'
  #   get 'desaltorizar'
  # end

  resources :tipo_equipamentos
  resources :tipo_documentos

  get 'prazos' => "registroprazos#index"
  get 'prazos/novo' => "registroprazos#new"

  resources :registroprazos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  root "tipo_documentos#home"
end
